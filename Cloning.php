<?php

  class Posts {
    public $post;

    function __construct($post) {
      $this->post = $post;
    }
  }

  echo "Section 1:<br>";
  $post1 = new Posts("First post");
  //Copy by reference (just copy pointer, not data)
  $post2 = $post1;
  $post1->post = "Modified post.";
  echo $post2->post . "<br>";

  echo "<br>Section 2:<br>";
  $post2->post = "Also modified post.";
  echo $post1->post . "<br>";

  echo "<br>Section 3:<br>";
  $post1 = new Posts("Mostly first post");
  $post2 = clone $post1;
  $post1->post = "Modified post.";
  echo $post2->post . "<br>";

  echo "<br>Section 3:<br>";
  $post2->post = "This is modified also!";
  echo $post1->post . "<br>";
  echo $post2->post . "<br>";
